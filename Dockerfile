# Containerfile taken from https://beachape.com/blog/2024/01/01/containerising-an-octopress-2-dot-0-blog/
# and stripped from the extra user created

FROM ubuntu:22.04

# System dependencies, including those required to install Ruby
RUN apt-get update -y && apt-get -y install software-properties-common
RUN add-apt-repository -y ppa:rael-gc/rvm && apt-get update -y && apt-get -y install \
  sudo \
  make \
  git \
  vim less \
  curl \
  build-essential \
  libreadline-dev \
  libssl1.0-dev \
  zlib1g-dev \
  python2.7 \
  imagemagick

# Install gcc-7 for Ruby 2.3 by adding and removing focal source
RUN cp /etc/apt/sources.list /etc/apt/sources.list.bak
RUN echo "deb [arch=amd64] http://archive.ubuntu.com/ubuntu focal main universe" >> /etc/apt/sources.list
RUN apt-get update -y && apt-get -y install g++-7
RUN mv -f /etc/apt/sources.list.bak /etc/apt/sources.list
RUN apt-get update -y

# We will need python to be python2
# But python-is-python2 is not a thing anymore...
# Taken from: https://philippe.bourgau.net/how-i-fixed-the-unknown-language-pygments-error-in-octopress/
RUN ln -sf /usr/bin/python2.7 /usr/bin/python

# Install ruby-build & ruby
WORKDIR ~/
RUN curl -L https://github.com/rbenv/ruby-build/archive/refs/tags/v20231225.tar.gz > ruby-build.tar.gz
RUN tar -xzf ruby-build.tar.gz 
RUN PREFIX=/usr/local ./ruby-build-*/install.sh
RUN CC=/usr/bin/gcc-7 ruby-build 2.3.3 /usr/local

# Initialise ruby encording
ENV RUBYOPT -EUTF-8

# Directory for the blog files
#RUN sudo mkdir /octopress
RUN mkdir /octopress
WORKDIR /octopress

# Add the Gemfile and install the gems
ADD Gemfile /octopress/Gemfile
ADD Gemfile.lock /octopress/Gemfile.lock
RUN gem update --system 3.2.3
RUN gem install bundler -v 2.3
RUN bundle install
