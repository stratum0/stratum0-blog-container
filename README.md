## Stratum 0 Blog Container

Das Stratum 0 Blog basiert auf Octopress von _anno dazumal_.
Damit wir es weiterhin ohne Änderung nutzen können muss ein System mit 
Ruby 2.7 und Python 2 (und `python-is-python2`) verfügbar sein.

Früher™ haben wir dafür die VM [bogomir](https://stratum0.org/wiki/Bogomir) bereit
gehalten. Mittlerweile ist das System aber so überholt, dass man es nicht
weiter als VM behalten möchte.

Mit diesem Container wickeln wir all die Legacy ein und bauen unseren Blog
darin einfach weiterhin als wäre nichts gewesen.
